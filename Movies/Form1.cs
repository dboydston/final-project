﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movies
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void ClearApplication()
        {
            dgvActors.Rows.Clear();
            dgvDirectors.Rows.Clear();
            dgvProducers.Rows.Clear();
            txtMovies.Clear();
            txtStudio.Clear();
            txtReleased.Clear();
            txtRunTime.Clear();
            lbLocations.Items.Clear();
        }
        private void cboMovies_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboMovies.SelectedIndex >= 0)
            {
                // Get the selected category.
                int intSelectedMovie = (int)cboMovies.SelectedValue;
                string strSelectedMovie = cboMovies.Text;
                // Fill the Datagridview manually.
                // Clear old data
                ClearApplication();

                using (var context = new MoviesEntities())
                {
                    var qMovie =
                        from Movie in context.tMovies
                        where Movie.fFilmID == intSelectedMovie
                        select Movie;
                    var qDirectorsinMovie =
                        from Movie in context.tRoles
                        where Movie.fFilmID == intSelectedMovie && Movie.fDirector
                        select Movie.fPeopleID;
                    var qDirectorsinRoles =
                         from Person in context.tPeoples
                         let fFullName = Person.fFirstName + " " + Person.fLastName
                         where qDirectorsinMovie.Contains(Person.fPeopleID)
                         select new { fFullName, Person.fPeopleID };
                    var qActorsinMovie =
                        from Movie in context.tRoles
                        where Movie.fFilmID == intSelectedMovie && Movie.fCastMember
                        select Movie.fPeopleID;
                    var qActorsinRoles =
                         from Person in context.tPeoples
                         let fFullName = Person.fFirstName + " " + Person.fLastName
                         where qActorsinMovie.Contains(Person.fPeopleID)
                         select new { fFullName, Person.fPeopleID };
                    var qProducersinMovie =
                        from Movie in context.tRoles
                        where Movie.fFilmID == intSelectedMovie && Movie.fProducer
                        select Movie.fPeopleID;
                    var qProducersinRoles =
                         from Person in context.tPeoples
                         let fFullName = Person.fFirstName + " " + Person.fLastName
                         where qProducersinMovie.Contains(Person.fPeopleID)
                         select new { fFullName, Person.fPeopleID };
                    var qLocationInMovie =
                        from Location in context.tFilmLocations
                        where Location.fFilmID == intSelectedMovie
                        select Location.fAddressID;
                    var qLocationInFilmLocation =
                        from Location in context.tAddresses
                        let fFullAddressName = Location.fCity + "," + " " + Location.fCountry
                        where qLocationInMovie.Contains(Location.fAddressID)
                        select new { fFullAddressName };
                    foreach (var movie in qMovie)
                    {
                        txtMovies.Text = movie.fTitle;
                        txtReleased.Text = movie.fYearRelease.ToShortDateString();
                        txtStudio.Text = movie.fStudio;
                        txtRunTime.Text = movie.fLength.ToString();
                    }
                    foreach (var Person in qDirectorsinRoles)
                    {
                        int iNewRowIndex = dgvDirectors.Rows.Add();
                        DataGridViewRow dgvrNewRow = dgvDirectors.Rows[iNewRowIndex];
                        dgvrNewRow.Cells["NameDirector"].Value = Person.fFullName;
                        dgvrNewRow.Cells["DirectorPersonID"].Value = Person.fPeopleID;
                    }
                    foreach (var Person in qActorsinRoles)
                    {
                        int iNewRowIndex = dgvActors.Rows.Add();
                        DataGridViewRow dgvrNewRow = dgvActors.Rows[iNewRowIndex];
                        dgvrNewRow.Cells["NameActor"].Value = Person.fFullName;
                        dgvrNewRow.Cells["ActorPersonID"].Value = Person.fPeopleID;
                    }
                    foreach (var Person in qProducersinRoles)
                    {
                        int iNewRowIndex = dgvProducers.Rows.Add();
                        DataGridViewRow dgvrNewRow = dgvProducers.Rows[iNewRowIndex];
                        dgvrNewRow.Cells["NameProducer"].Value = Person.fFullName;
                        dgvrNewRow.Cells["ProducerPersonID"].Value = Person.fPeopleID;
                    }
                    foreach (var Location in qLocationInFilmLocation)
                    {
                       lbLocations.Items.Add(Location.fFullAddressName);
                    }
                }
            }
        }

        private void cboPerson_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cboPerson.SelectedIndex >= 0)
            {
                cboMovies.DataSource = null;
                cboMovies.Items.Clear();
                int intPersonSelected = (int)cboPerson.SelectedValue;
                using (var context = new MoviesEntities())
                {
                    if (rdbNoFilter.Checked)
                    {
                        var qPerson =
                            from Person in context.tRoles
                            where Person.fPeopleID == intPersonSelected
                            select Person.fFilmID;
                        var qMovies =
                            from Movie in context.tMovies
                            where qPerson.Contains(Movie.fFilmID)
                            select Movie;
                    }
                    else if (rdbDirector.Checked)
                    {
                        var qPersonDirector =
                            from Person in context.tRoles
                            where Person.fPeopleID == intPersonSelected && Person.fDirector
                            select Person.fFilmID;
                        var qMovies =
                            from Movie in context.tMovies
                            where qPersonDirector.Contains(Movie.fFilmID)
                            select Movie;
                        foreach (var Movie in qMovies)
                        {
                            cboMovies.DataSource = qMovies.ToList();
                            cboMovies.DisplayMember = "fTitle";
                            cboMovies.ValueMember = "fFilmID";
                        }
                    }
                    else if (rdbActor.Checked)
                    {
                        var qPersonActor =
                            from Person in context.tRoles
                            where Person.fPeopleID == intPersonSelected && Person.fCastMember
                            select Person.fFilmID;
                        var qMovies =
                            from Movie in context.tMovies
                            where qPersonActor.Contains(Movie.fFilmID)
                            select Movie;
                        foreach (var Movie in qMovies)
                        {
                            cboMovies.DataSource = qMovies.ToList();
                            cboMovies.DisplayMember = "fTitle";
                            cboMovies.ValueMember = "fFilmID";
                        }
                    }
                    else if (rdbProducer.Checked)
                    {
                        var qPersonProducer =
                            from Person in context.tRoles
                            where Person.fPeopleID == intPersonSelected && Person.fProducer
                            select Person.fFilmID;
                        var qMovies =
                            from Movie in context.tMovies
                            where qPersonProducer.Contains(Movie.fFilmID)
                            select Movie;
                        foreach (var Movie in qMovies)
                        {
                            cboMovies.DataSource = qMovies.ToList();
                            cboMovies.DisplayMember = "fTitle";
                            cboMovies.ValueMember = "fFilmID";
                        }
                    }
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //using (var context = new MoviesEntities())
            //{
            //    var qPeople =
            //        from People in context.tPeoples
            //        orderby People.fLastName
            //        select People;
            //    cboPerson.DataSource = qPeople.ToList();
            //    cboPerson.DisplayMember = "People.fFullName";
            //    cboPerson.ValueMember = "fPeopleID";
            //}
            rdbNoFilter_CheckedChanged(null, null);
        }

        private void rdbNoFilter_CheckedChanged(object sender, EventArgs e)
        {
           if (rdbNoFilter.Checked)
            {
                cboPerson.DataSource = null;
                cboPerson.Items.Clear();
                using (var context = new MoviesEntities())
                {
                    var qMovies =
                    from Movies in context.tMovies
                    select Movies;

                    cboMovies.DisplayMember = "fTitle";
                    cboMovies.ValueMember = "fFilmID";
                    cboMovies.DataSource = qMovies.ToList();
                }
            }
        }

        private void rdbDirector_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbDirector.Checked)
            {
                cboPerson.DataSource = null;
                cboPerson.Items.Clear();
                using (var context = new MoviesEntities())
                {
                    var qRoles =
                        from People in context.tRoles
                        where People.fDirector
                        select People.fPeopleID;
                    var qPeople =
                        from People in context.tPeoples
                        let fFullName = People.fFirstName + " " + People.fLastName
                        orderby People.fLastName
                        where qRoles.Contains(People.fPeopleID)
                        select new { fFullName, People.fPeopleID };


                    cboPerson.DisplayMember = "fFullName";
                    cboPerson.ValueMember = "fPeopleID";
                    cboPerson.DataSource = qPeople.ToList();
                }
            }
        }

        private void rdbProducer_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbProducer.Checked)
            {
                cboPerson.DataSource = null;
                cboPerson.Items.Clear();
                using (var context = new MoviesEntities())
                {
                    var qRoles =
                        from People in context.tRoles
                        where People.fProducer
                        select People.fPeopleID;
                    var qPeople =
                        from People in context.tPeoples
                        let fFullName = People.fFirstName + " " + People.fLastName
                        orderby People.fLastName
                        where qRoles.Contains(People.fPeopleID)
                        select new { fFullName, People.fPeopleID };


                    cboPerson.DisplayMember = "fFullName";
                    cboPerson.ValueMember = "fPeopleID";
                    cboPerson.DataSource = qPeople.ToList();
                }
            }
        }

        private void rdbActor_CheckedChanged(object sender, EventArgs e)
        {
            if (rdbActor.Checked)
            {
                cboPerson.DataSource = null;
                cboPerson.Items.Clear();
                using (var context = new MoviesEntities())
                {
                    var qRoles =
                        from People in context.tRoles
                        where People.fCastMember
                        select People.fPeopleID;
                    var qPeople =
                        from People in context.tPeoples
                        let fFullName = People.fFirstName + " " + People.fLastName
                        orderby People.fLastName
                        where qRoles.Contains(People.fPeopleID)
                        select new { fFullName, People.fPeopleID };


                    cboPerson.DisplayMember = "fFullName";
                    cboPerson.ValueMember = "fPeopleID";
                    cboPerson.DataSource = qPeople.ToList();
                }
            }
        }

        private void btnDirector_Click(object sender, EventArgs e)
        {
            using (var context = new MoviesEntities())
            {
                DataGridViewRow dgvrSelectedRow = dgvDirectors.SelectedRows[0];
                int intSelectedMovie = (int)cboMovies.SelectedValue;
                int intSelectedPerson = (int)dgvrSelectedRow.Cells["DirectorPersonID"].Value;
                var qPersonDirector =
                    from Person in context.tRoles
                    where Person.fPeopleID == intSelectedPerson && Person.fDirector
                    select Person.fFilmID;
                var qMovie =
                    from Movie in context.tMovies
                    where qPersonDirector.Contains(Movie.fFilmID) && Movie.fFilmID != intSelectedMovie
                    select Movie;

                InformationForm frmInfo = new InformationForm();
                frmInfo.PersonName = dgvDirectors.CurrentCell.Value.ToString();
                frmInfo.RoleType = "Other movies directed:";
                foreach (var Movie in qMovie)
                {
                    frmInfo.ListInformation = Movie.fTitle;
                }
                frmInfo.ShowDialog();
            }


        }

        private void btnProducer_Click(object sender, EventArgs e)
        {
            using (var context = new MoviesEntities())
            {
                DataGridViewRow dgvrSelectedRow = dgvProducers.SelectedRows[0];
                int intSelectedMovie = (int)cboMovies.SelectedValue;
                int intSelectedPerson = (int)dgvrSelectedRow.Cells["ProducerPersonID"].Value;
                var qPersonProducer =
                    from Person in context.tRoles
                    where Person.fPeopleID == intSelectedPerson && Person.fProducer
                    select Person.fFilmID;
                var qMovie =
                    from Movie in context.tMovies
                    where qPersonProducer.Contains(Movie.fFilmID) && Movie.fFilmID != intSelectedMovie
                    select Movie;
                InformationForm frmInfo = new InformationForm();
                frmInfo.PersonName = dgvProducers.CurrentCell.Value.ToString();
                frmInfo.RoleType = "Other movies produced:";
                foreach (var Movie in qMovie)
                {
                    frmInfo.ListInformation = Movie.fTitle;
                }
                frmInfo.ShowDialog();
            }
        }

        private void btnActor_Click(object sender, EventArgs e)
        {
            using (var context = new MoviesEntities())
            {
                DataGridViewRow dgvrSelectedRow = dgvActors.SelectedRows[0];
                int intSelectedMovie = (int)cboMovies.SelectedValue;
                int intSelectedPerson = (int)dgvrSelectedRow.Cells["ActorPersonID"].Value;
                var qPersonActor =
                    from Person in context.tRoles
                    where Person.fPeopleID == intSelectedPerson && Person.fCastMember
                    select Person.fFilmID;
                var qMovie =
                    from Movie in context.tMovies
                    where qPersonActor.Contains(Movie.fFilmID) && Movie.fFilmID != intSelectedMovie
                    select Movie;

                InformationForm frmInfo = new InformationForm();
                frmInfo.PersonName = dgvActors.CurrentCell.Value.ToString();
                frmInfo.RoleType = "Other movies appeared in:";
                foreach (var Movie in qMovie)
                {
                    frmInfo.ListInformation = Movie.fTitle;
                }
                frmInfo.ShowDialog();
            }
        }
    }
}
