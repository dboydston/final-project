﻿namespace Movies
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mFileMenu = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.rdbNoFilter = new System.Windows.Forms.RadioButton();
            this.rdbDirector = new System.Windows.Forms.RadioButton();
            this.rdbProducer = new System.Windows.Forms.RadioButton();
            this.rdbActor = new System.Windows.Forms.RadioButton();
            this.lblMovies = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cboMovies = new System.Windows.Forms.ComboBox();
            this.cboPerson = new System.Windows.Forms.ComboBox();
            this.txtMovies = new System.Windows.Forms.TextBox();
            this.txtReleased = new System.Windows.Forms.TextBox();
            this.txtStudio = new System.Windows.Forms.TextBox();
            this.txtRunTime = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lbLocations = new System.Windows.Forms.ListBox();
            this.dgvDirectors = new System.Windows.Forms.DataGridView();
            this.dgvActors = new System.Windows.Forms.DataGridView();
            this.dgvProducers = new System.Windows.Forms.DataGridView();
            this.btnDirector = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.btnProducer = new System.Windows.Forms.Button();
            this.btnActor = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.NameProducer = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ProducerPersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameDirector = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.DirectorPersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NameActor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ActorPersonID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.mFileMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDirectors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActors)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducers)).BeginInit();
            this.SuspendLayout();
            // 
            // mFileMenu
            // 
            this.mFileMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.mFileMenu.Location = new System.Drawing.Point(0, 0);
            this.mFileMenu.Name = "mFileMenu";
            this.mFileMenu.Size = new System.Drawing.Size(822, 24);
            this.mFileMenu.TabIndex = 0;
            this.mFileMenu.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(92, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // rdbNoFilter
            // 
            this.rdbNoFilter.AutoSize = true;
            this.rdbNoFilter.Location = new System.Drawing.Point(12, 42);
            this.rdbNoFilter.Name = "rdbNoFilter";
            this.rdbNoFilter.Size = new System.Drawing.Size(61, 17);
            this.rdbNoFilter.TabIndex = 1;
            this.rdbNoFilter.TabStop = true;
            this.rdbNoFilter.Text = "No filter";
            this.rdbNoFilter.UseVisualStyleBackColor = true;
            this.rdbNoFilter.CheckedChanged += new System.EventHandler(this.rdbNoFilter_CheckedChanged);
            // 
            // rdbDirector
            // 
            this.rdbDirector.AutoSize = true;
            this.rdbDirector.Location = new System.Drawing.Point(12, 65);
            this.rdbDirector.Name = "rdbDirector";
            this.rdbDirector.Size = new System.Drawing.Size(101, 17);
            this.rdbDirector.TabIndex = 2;
            this.rdbDirector.TabStop = true;
            this.rdbDirector.Text = "Filter by Director";
            this.rdbDirector.UseVisualStyleBackColor = true;
            this.rdbDirector.CheckedChanged += new System.EventHandler(this.rdbDirector_CheckedChanged);
            // 
            // rdbProducer
            // 
            this.rdbProducer.AutoSize = true;
            this.rdbProducer.Location = new System.Drawing.Point(12, 88);
            this.rdbProducer.Name = "rdbProducer";
            this.rdbProducer.Size = new System.Drawing.Size(107, 17);
            this.rdbProducer.TabIndex = 3;
            this.rdbProducer.TabStop = true;
            this.rdbProducer.Text = "Filter by Producer";
            this.rdbProducer.UseVisualStyleBackColor = true;
            this.rdbProducer.CheckedChanged += new System.EventHandler(this.rdbProducer_CheckedChanged);
            // 
            // rdbActor
            // 
            this.rdbActor.AutoSize = true;
            this.rdbActor.Location = new System.Drawing.Point(12, 111);
            this.rdbActor.Name = "rdbActor";
            this.rdbActor.Size = new System.Drawing.Size(89, 17);
            this.rdbActor.TabIndex = 4;
            this.rdbActor.TabStop = true;
            this.rdbActor.Text = "Filter by Actor";
            this.rdbActor.UseVisualStyleBackColor = true;
            this.rdbActor.CheckedChanged += new System.EventHandler(this.rdbActor_CheckedChanged);
            // 
            // lblMovies
            // 
            this.lblMovies.AutoSize = true;
            this.lblMovies.Location = new System.Drawing.Point(12, 149);
            this.lblMovies.Name = "lblMovies";
            this.lblMovies.Size = new System.Drawing.Size(41, 13);
            this.lblMovies.TabIndex = 5;
            this.lblMovies.Text = "Movies";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 209);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(40, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Person";
            // 
            // cboMovies
            // 
            this.cboMovies.FormattingEnabled = true;
            this.cboMovies.Location = new System.Drawing.Point(12, 165);
            this.cboMovies.Name = "cboMovies";
            this.cboMovies.Size = new System.Drawing.Size(121, 21);
            this.cboMovies.TabIndex = 7;
            this.cboMovies.SelectedIndexChanged += new System.EventHandler(this.cboMovies_SelectedIndexChanged);
            // 
            // cboPerson
            // 
            this.cboPerson.FormattingEnabled = true;
            this.cboPerson.Location = new System.Drawing.Point(12, 225);
            this.cboPerson.Name = "cboPerson";
            this.cboPerson.Size = new System.Drawing.Size(121, 21);
            this.cboPerson.TabIndex = 8;
            this.cboPerson.SelectedIndexChanged += new System.EventHandler(this.cboPerson_SelectedIndexChanged);
            // 
            // txtMovies
            // 
            this.txtMovies.Location = new System.Drawing.Point(252, 62);
            this.txtMovies.Name = "txtMovies";
            this.txtMovies.ReadOnly = true;
            this.txtMovies.Size = new System.Drawing.Size(133, 20);
            this.txtMovies.TabIndex = 9;
            // 
            // txtReleased
            // 
            this.txtReleased.Location = new System.Drawing.Point(267, 146);
            this.txtReleased.Name = "txtReleased";
            this.txtReleased.ReadOnly = true;
            this.txtReleased.Size = new System.Drawing.Size(76, 20);
            this.txtReleased.TabIndex = 10;
            // 
            // txtStudio
            // 
            this.txtStudio.Location = new System.Drawing.Point(267, 108);
            this.txtStudio.Name = "txtStudio";
            this.txtStudio.ReadOnly = true;
            this.txtStudio.Size = new System.Drawing.Size(118, 20);
            this.txtStudio.TabIndex = 11;
            // 
            // txtRunTime
            // 
            this.txtRunTime.Location = new System.Drawing.Point(267, 182);
            this.txtRunTime.Name = "txtRunTime";
            this.txtRunTime.ReadOnly = true;
            this.txtRunTime.Size = new System.Drawing.Size(76, 20);
            this.txtRunTime.TabIndex = 13;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(202, 115);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 13);
            this.label1.TabIndex = 14;
            this.label1.Text = "Studio:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(202, 153);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(55, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "Released:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(202, 189);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 13);
            this.label4.TabIndex = 16;
            this.label4.Text = "Runtime:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(202, 228);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(53, 13);
            this.label6.TabIndex = 19;
            this.label6.Text = "Locations";
            // 
            // lbLocations
            // 
            this.lbLocations.FormattingEnabled = true;
            this.lbLocations.Location = new System.Drawing.Point(267, 225);
            this.lbLocations.Name = "lbLocations";
            this.lbLocations.Size = new System.Drawing.Size(142, 69);
            this.lbLocations.TabIndex = 20;
            // 
            // dgvDirectors
            // 
            this.dgvDirectors.AllowUserToAddRows = false;
            this.dgvDirectors.AllowUserToDeleteRows = false;
            this.dgvDirectors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDirectors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameDirector,
            this.DirectorPersonID});
            this.dgvDirectors.Location = new System.Drawing.Point(472, 52);
            this.dgvDirectors.Name = "dgvDirectors";
            this.dgvDirectors.ReadOnly = true;
            this.dgvDirectors.RowHeadersVisible = false;
            this.dgvDirectors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvDirectors.Size = new System.Drawing.Size(208, 86);
            this.dgvDirectors.TabIndex = 21;
            // 
            // dgvActors
            // 
            this.dgvActors.AllowUserToAddRows = false;
            this.dgvActors.AllowUserToDeleteRows = false;
            this.dgvActors.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvActors.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameActor,
            this.ActorPersonID});
            this.dgvActors.Location = new System.Drawing.Point(472, 308);
            this.dgvActors.Name = "dgvActors";
            this.dgvActors.ReadOnly = true;
            this.dgvActors.RowHeadersVisible = false;
            this.dgvActors.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvActors.Size = new System.Drawing.Size(208, 86);
            this.dgvActors.TabIndex = 22;
            // 
            // dgvProducers
            // 
            this.dgvProducers.AllowUserToAddRows = false;
            this.dgvProducers.AllowUserToDeleteRows = false;
            this.dgvProducers.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvProducers.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.NameProducer,
            this.ProducerPersonID});
            this.dgvProducers.Location = new System.Drawing.Point(472, 182);
            this.dgvProducers.Name = "dgvProducers";
            this.dgvProducers.ReadOnly = true;
            this.dgvProducers.RowHeadersVisible = false;
            this.dgvProducers.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvProducers.Size = new System.Drawing.Size(208, 86);
            this.dgvProducers.TabIndex = 23;
            // 
            // btnDirector
            // 
            this.btnDirector.Location = new System.Drawing.Point(706, 82);
            this.btnDirector.Name = "btnDirector";
            this.btnDirector.Size = new System.Drawing.Size(75, 23);
            this.btnDirector.TabIndex = 24;
            this.btnDirector.Text = "Show Info";
            this.btnDirector.UseVisualStyleBackColor = true;
            this.btnDirector.Click += new System.EventHandler(this.btnDirector_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(551, 36);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(49, 13);
            this.label5.TabIndex = 25;
            this.label5.Text = "Directors";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(551, 165);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Producers";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(557, 281);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(37, 13);
            this.label8.TabIndex = 27;
            this.label8.Text = "Actors";
            // 
            // btnProducer
            // 
            this.btnProducer.Location = new System.Drawing.Point(706, 209);
            this.btnProducer.Name = "btnProducer";
            this.btnProducer.Size = new System.Drawing.Size(75, 23);
            this.btnProducer.TabIndex = 28;
            this.btnProducer.Text = "Show Info";
            this.btnProducer.UseVisualStyleBackColor = true;
            this.btnProducer.Click += new System.EventHandler(this.btnProducer_Click);
            // 
            // btnActor
            // 
            this.btnActor.Location = new System.Drawing.Point(706, 338);
            this.btnActor.Name = "btnActor";
            this.btnActor.Size = new System.Drawing.Size(75, 23);
            this.btnActor.TabIndex = 29;
            this.btnActor.Text = "Show Info";
            this.btnActor.UseVisualStyleBackColor = true;
            this.btnActor.Click += new System.EventHandler(this.btnActor_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(212, 65);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(30, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Title:";
            // 
            // NameProducer
            // 
            this.NameProducer.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameProducer.HeaderText = "Name";
            this.NameProducer.Name = "NameProducer";
            this.NameProducer.ReadOnly = true;
            // 
            // ProducerPersonID
            // 
            this.ProducerPersonID.HeaderText = "Producer";
            this.ProducerPersonID.Name = "ProducerPersonID";
            this.ProducerPersonID.ReadOnly = true;
            this.ProducerPersonID.Visible = false;
            // 
            // NameDirector
            // 
            this.NameDirector.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameDirector.HeaderText = "Name";
            this.NameDirector.Name = "NameDirector";
            this.NameDirector.ReadOnly = true;
            // 
            // DirectorPersonID
            // 
            this.DirectorPersonID.HeaderText = "Director";
            this.DirectorPersonID.Name = "DirectorPersonID";
            this.DirectorPersonID.ReadOnly = true;
            this.DirectorPersonID.Visible = false;
            // 
            // NameActor
            // 
            this.NameActor.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.NameActor.HeaderText = "Name";
            this.NameActor.Name = "NameActor";
            this.NameActor.ReadOnly = true;
            // 
            // ActorPersonID
            // 
            this.ActorPersonID.HeaderText = "Actor";
            this.ActorPersonID.Name = "ActorPersonID";
            this.ActorPersonID.ReadOnly = true;
            this.ActorPersonID.Visible = false;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(822, 426);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.btnActor);
            this.Controls.Add(this.btnProducer);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnDirector);
            this.Controls.Add(this.dgvProducers);
            this.Controls.Add(this.dgvActors);
            this.Controls.Add(this.dgvDirectors);
            this.Controls.Add(this.lbLocations);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtRunTime);
            this.Controls.Add(this.txtStudio);
            this.Controls.Add(this.txtReleased);
            this.Controls.Add(this.txtMovies);
            this.Controls.Add(this.cboPerson);
            this.Controls.Add(this.cboMovies);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblMovies);
            this.Controls.Add(this.rdbActor);
            this.Controls.Add(this.rdbProducer);
            this.Controls.Add(this.rdbDirector);
            this.Controls.Add(this.rdbNoFilter);
            this.Controls.Add(this.mFileMenu);
            this.MainMenuStrip = this.mFileMenu;
            this.Name = "Form1";
            this.Text = "Movies";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.mFileMenu.ResumeLayout(false);
            this.mFileMenu.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDirectors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvActors)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvProducers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip mFileMenu;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.RadioButton rdbNoFilter;
        private System.Windows.Forms.RadioButton rdbDirector;
        private System.Windows.Forms.RadioButton rdbProducer;
        private System.Windows.Forms.RadioButton rdbActor;
        private System.Windows.Forms.Label lblMovies;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cboMovies;
        private System.Windows.Forms.ComboBox cboPerson;
        private System.Windows.Forms.TextBox txtMovies;
        private System.Windows.Forms.TextBox txtReleased;
        private System.Windows.Forms.TextBox txtStudio;
        private System.Windows.Forms.TextBox txtRunTime;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListBox lbLocations;
        private System.Windows.Forms.DataGridView dgvDirectors;
        private System.Windows.Forms.DataGridView dgvActors;
        private System.Windows.Forms.DataGridView dgvProducers;
        private System.Windows.Forms.Button btnDirector;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnProducer;
        private System.Windows.Forms.Button btnActor;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameDirector;
        private System.Windows.Forms.DataGridViewTextBoxColumn DirectorPersonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameActor;
        private System.Windows.Forms.DataGridViewTextBoxColumn ActorPersonID;
        private System.Windows.Forms.DataGridViewTextBoxColumn NameProducer;
        private System.Windows.Forms.DataGridViewTextBoxColumn ProducerPersonID;
    }
}

