﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Movies
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class MoviesEntities : DbContext
    {
        public MoviesEntities()
            : base("name=MoviesEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<tAdditionalName> tAdditionalNames { get; set; }
        public virtual DbSet<tAddress> tAddresses { get; set; }
        public virtual DbSet<tFilmLocation> tFilmLocations { get; set; }
        public virtual DbSet<tMovies> tMovies { get; set; }
        public virtual DbSet<tPeople> tPeoples { get; set; }
        public virtual DbSet<tRole> tRoles { get; set; }
    }
}
