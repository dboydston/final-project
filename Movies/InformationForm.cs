﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Movies
{
    public partial class InformationForm : Form
    {
        public InformationForm()
        {
            InitializeComponent();
        }
        public string ListInformation
        {
            set
            {
                int iNewRowIndex = dgvMovies.Rows.Add();
                DataGridViewRow dgvrNewRow = dgvMovies.Rows[iNewRowIndex];
                dgvrNewRow.Cells["MovieColumn"].Value = value;
            }
        }
        public string PersonName
        {
            set
            {
                txtName.Text = value;
            }
        }
        public string RoleType
        {
            set
            {
                lblMovieInfo.Text = value;
            }
        }

        private void exitToolStripMenuItem_Click_1(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
